package com.example.base

import android.os.Bundle
import gurtek.mrgurtekbase.KotlinBaseActivity

class MainActivity : KotlinBaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
