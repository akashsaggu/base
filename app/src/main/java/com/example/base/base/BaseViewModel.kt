package com.example.base.base

import com.example.base.networking.model.CommonResponse
import com.example.base.repository.RemoteRepository
import com.example.base.util.rxkotlin.addToDisposable
import com.example.base.util.rxkotlin.useProgress
import gurtek.mrgurtekbase.viewmodel.KotlinBaseViewModel
import gurtek.mrgurtekbase.viewmodel.common.ErrorType
import io.reactivex.Single
import org.koin.core.KoinComponent
import org.koin.core.inject

open class BaseViewModel : KotlinBaseViewModel(), KoinComponent {
    private val remoteRepository: RemoteRepository by inject()

    fun getRemoteRepo(): RemoteRepository {
        return remoteRepository
    }

    fun <T> Single<out T>.subscribeWithDisposable(success: ((T) -> Unit)? = null) where T : CommonResponse {
        subscribe({
            when (it.status) {
                true -> success?.invoke(it)
                false -> showError(it.message, ErrorType.SNACKBAR)
            }
        }, {
            showError(it?.message ?: "Sorry, something went wrong.", ErrorType.SNACKBAR)
        }).addToDisposable(disposables)
    }

    fun <T> Single<out T>.subscribeWithProgressAndDisposable(success: ((T) -> Unit)? = null) where T : CommonResponse {
        useProgress(this@BaseViewModel)
            .subscribeWithDisposable(success)
    }

}