package com.example.base.di

import com.example.base.base.BaseViewModel
import com.example.base.repository.RemoteRepository
import com.example.base.ui.edit.vm.EditProfileVm
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModels = module {
    viewModel { BaseViewModel() }
    viewModel { EditProfileVm() }
}

val common = module {
    single { RemoteRepository() }
}