package com.example.base.networking.model


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


data class UserModel(
    @SerializedName("data")
    val `data`: Data
) : CommonResponse() {
    @Parcelize
    data class Data(
        @SerializedName("email_topic")
        val emailTopic: String,
        @SerializedName("payment_valid_from")
        val paymentValidFrom: String,
        @SerializedName("payment_valid_till")
        val paymentValidTill: String,
        @SerializedName("push_topic")
        val pushTopic: String,
        @SerializedName("ratings")
        val ratings: String,
        @SerializedName("u_added_on")
        val uAddedOn: String,
        @SerializedName("u_bio")
        val uBio: String,
        @SerializedName("u_birth_year")
        val uBirthYear: String,
        @SerializedName("u_cars")
        var uCars: List<UCar>,
        @SerializedName("u_doc_status")
        val uDocStatus: String,
        @SerializedName("u_email")
        val uEmail: String,
        @SerializedName("u_email_verified")
        val uEmailVerified: String,
        @SerializedName("u_first_name")
        var uFirstName: String,
        @SerializedName("u_gender")
        val uGender: String,
        @SerializedName("u_id")
        val uId: String,
        @SerializedName("u_last_name")
        var uLastName: String,
        @SerializedName("u_login_source")
        val uLoginSource: String,
        @SerializedName("u_password")
        val uPassword: String,
        @SerializedName("u_phone")
        val uPhone: String,
        @SerializedName("u_postal_address")
        val uPostalAddress: String,
        @SerializedName("u_profile_pic")
        val uProfilePic: String,
        @SerializedName("u_status")
        val uStatus: String,
        @SerializedName("u_updated_on")
        val uUpdatedOn: String,
        @SerializedName("user_chattiness")
        val userChattiness: String,
        @SerializedName("user_email_otp")
        val userEmailOtp: String,
        @SerializedName("user_music")
        val userMusic: String,
        @SerializedName("user_parent")
        val userParent: String,
        @SerializedName("user_payment_status")
        val userPaymentStatus: String,
        @SerializedName("user_pets")
        val userPets: String,
        @SerializedName("user_refferal")
        val userRefferal: String,
        @SerializedName("user_smoking")
        val userSmoking: String
    ) : Parcelable {
        @Parcelize
        data class UCar(
            @SerializedName("car_added_on")
            val carAddedOn: String,
            @SerializedName("car_body_type")
            val carBodyType: String,
            @SerializedName("car_brand")
            val carBrand: String,
            @SerializedName("car_color")
            val carColor: String,
            @SerializedName("car_id")
            val carId: String,
            @SerializedName("car_model")
            val carModel: String,
            @SerializedName("car_number_plate")
            val carNumberPlate: String,
            @SerializedName("car_photo_url")
            val carPhotoUrl: String,
            @SerializedName("car_reg_year")
            val carRegYear: String,
            @SerializedName("car_status")
            val carStatus: String,
            @SerializedName("car_updated_on")
            val carUpdatedOn: String,
            @SerializedName("car_user_id")
            val carUserId: String
        ) : Parcelable
    }
}
