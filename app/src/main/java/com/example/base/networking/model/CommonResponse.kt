package com.example.base.networking.model

import com.google.gson.annotations.SerializedName


open class CommonResponse {
    @SerializedName("Message")
    var message: String = ""
    @SerializedName("Status")
    var status: Boolean = false
}