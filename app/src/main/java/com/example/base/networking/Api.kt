package com.example.base.networking

import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit

class Api {
    fun createService(): ApiInterface {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
            .callTimeout(3, TimeUnit.MINUTES)
            .connectTimeout(3, TimeUnit.MINUTES)
            .readTimeout(3, TimeUnit.MINUTES)
            .writeTimeout(3, TimeUnit.MINUTES)
            .addInterceptor(interceptor)
        val retrofit = Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
            .client(client.build())
            .build()
        return retrofit.create(ApiInterface::class.java)
    }

    companion object {
        private const val API_BASE_URL = "http://stsmentor.com/zozocar_admin/api/v1/"
        val instance by lazy { Api().createService() }
        fun getService() = instance
    }

}

fun apiHitter(): ApiInterface {
    return Api.getService()
}


inline fun <reified T> T.toMultiPart(): MultipartBody {
    val builder = MultipartBody.Builder()
    builder.setType(MultipartBody.FORM)

    T::class.java.declaredFields.forEach {
        it.isAccessible = true
        if (it.get(this) != null)
            when (val fieldObj = it.get(this)) {
                is String -> builder.addFormDataPart(it.name, fieldObj)
                is File -> builder.addFormDataPart(
                    it.name,
                    it.name + System.currentTimeMillis() + ".jpg",
                    RequestBody.create(MediaType.parse("multipart/form-data"), fieldObj)
                )
                else -> builder.addFormDataPart(it.name, fieldObj.toString())
            }

        it.isAccessible = false
    }

    return builder.build()
}