package com.example.base.networking.postModel

import java.io.File

data class ProfilePicModel(var user_id: String, var profile_pic: File)