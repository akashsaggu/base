package com.example.base.networking

import com.example.base.networking.model.UserModel
import io.reactivex.Single
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST


interface ApiInterface {

    companion object {
        const val KEY = "zozo@123*"
    }


    @POST("users/updateprofilepic")
    fun uploadProfilePic(@Body request: RequestBody): Single<UserModel>

    @FormUrlEncoded
    @POST("users/addemail")
    fun addEmail(@Field("user_id") userId: String, @Field("user_email") userEmail: String): Single<UserModel>

    /*  @GET
      fun sendOtp(
          @Url url: String = "https://www.pay2all.in/web-api/send_sms?api_token=BvweAoSec0F7Gnlj6cklaiR5fX9lGSs40jwem6u3v3xoLjp4SEUFtiJzIvlJ&senderid=PAYTWO&route=1", @Query(
              "number"
          ) mobile: String, @Query("message") otp: String
      ): Completable*/

}