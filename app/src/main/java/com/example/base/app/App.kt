package com.example.base.app

import android.app.Application

import com.example.base.di.common
import com.example.base.di.viewModels
import com.example.base.util.preference.SharedPreferenceUtils
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


/**
 * Created by Akash Saggu(R4X)
 */

class App : Application() {

    val preference by lazy { SharedPreferenceUtils(this) }

    override fun onCreate() {
        super.onCreate()
        application = this

        startKoin {
            androidContext(this@App)
            modules(listOf(viewModels, common))
        }
    }

    companion object {
        lateinit var application: App
        @JvmStatic
        fun getApp() = application
    }

}

fun Any.getPref(): SharedPreferenceUtils {
    return App.getApp().preference
}