package com.example.base.util.recyclerview

interface ItemClickListener<T> {
    fun onItemClick(item: T)
}