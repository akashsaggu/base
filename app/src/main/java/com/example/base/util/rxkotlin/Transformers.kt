package com.example.base.util.rxkotlin

import gurtek.mrgurtekbase.viewmodel.KotlinBaseViewModel
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by Akash Saggu(R4X) on 13-04-2018.
 */
object Transformers {

    fun <T> applyItoMonO(): ObservableTransformer<T, T> {

        return ObservableTransformer {
            it.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        }

    }

    fun <T> applyCtoMonO(): ObservableTransformer<T, T> {

        return ObservableTransformer {
            it.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
        }

    }

    fun <T> applyCtoIonO(): ObservableTransformer<T, T> {

        return ObservableTransformer {
            it.subscribeOn(Schedulers.computation())
                .observeOn(Schedulers.io())

        }

    }


    fun <T> applyItoMonF(): FlowableTransformer<T, T> {

        return FlowableTransformer {
            it.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(2)
        }

    }

    fun <T> applyCtoMonF(): FlowableTransformer<T, T> {

        return FlowableTransformer {
            it.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
        }

    }

    fun <T> applyItoMonS(): SingleTransformer<T, T> {

        return SingleTransformer {
            it.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        }

    }

    fun <T> applyCtoMonS(): SingleTransformer<T, T> {

        return SingleTransformer {
            it.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
        }

    }
}

fun <T> Single<T>.onNetwork(): Single<T> {
    return compose(Transformers.applyItoMonS())
}

fun <T> Single<T>.useProgress(viewModel: KotlinBaseViewModel): Single<T> {
    return compose {
        it.doOnSubscribe {
            viewModel.showProgress()
        }.doFinally { viewModel.hideProgress() }.doOnSuccess { viewModel.hideProgress() }
            .doOnError { viewModel.hideProgress() }.doOnTerminate { viewModel.hideProgress() }
    }
}

fun <T> Observable<T>.useProgress(viewModel: KotlinBaseViewModel): Observable<T> {
    return compose {
        it.doOnSubscribe {
            viewModel.showProgress()
        }
            .doFinally { viewModel.hideProgress() }
            .doOnComplete { viewModel.hideProgress() }
            .doOnError { viewModel.hideProgress() }
            .doOnTerminate { viewModel.hideProgress() }
    }
}

fun <T> Single<T>.onNetworkWithProgress(viewModel: KotlinBaseViewModel): Single<T> {
    return onNetwork().useProgress(viewModel)

}

fun Disposable.addToDisposable(disposable: CompositeDisposable?) {
    disposable?.add(this)
}

