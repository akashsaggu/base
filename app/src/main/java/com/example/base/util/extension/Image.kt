package com.example.base.util.extension

import android.animation.Animator
import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.view.View
import android.view.animation.PathInterpolator
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.base.R
import java.io.ByteArrayOutputStream
import java.io.IOException


/**
 * Created by Akash Saggu(R4X) on 12/9/18 at 18:54.
 * akashsaggu@protonmail.com
 * @Version 1 (12/9/18)
 * @Updated on 12/9/18
 */

inline fun <reified T> ImageView.loadImage(
    source: T,
    @DrawableRes errorImage: Int = R.drawable.profile_default
) {
    when (T::class) {
        String::class -> {
            val image = source.run {
                if ((this as String).contains(
                        "http",
                        true
                    )
                ) this else "http://stsmentor.com/zozocar_admin/$source"
            }
            Glide.with(this.context)
                .load(image)
                .apply(requestOptions(errorImage))
                .into(this)
        }
        Uri::class -> Glide.with(this.context)
            .load(source as Uri)
            .into(this)
        else -> Glide.with(this.context)
            .load(source)
            .into(this)
    }

}

fun ImageView.requestOptions(errorImage: Int): RequestOptions {
    return RequestOptions()
        .useUnlimitedSourceGeneratorsPool(true)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .centerCrop()
        .placeholder(errorImage)
        .override(width, height)
}

/*
inline fun palletListener(view: ImageView, crossinline body: (Palette) -> Unit): BitmapImageViewTarget {
    return object : BitmapImageViewTarget(view) {
        override fun onResourceReady(resource: Bitmap, transition: com.bumptech.glide.request.transition.Transition<in Bitmap>?) {
            super.onResourceReady(resource, transition)
            Palette.from(resource).generate { palette: Palette ->
                body.invoke(palette)
            }
        }
    }
}*/

fun View.createBackgroundColorTransition(@ColorInt startColor: Int, @ColorInt endColor: Int): Animator {
    return createColorAnimator(this, "backgroundColor", startColor, endColor)
}

fun TextView.createTextColorTransition(@ColorInt startColor: Int, @ColorInt endColor: Int): Animator {
    return createColorAnimator(this, "textColor", startColor, endColor)
}

private fun createColorAnimator(
    target: Any,
    propertyName: String, @ColorInt startColor: Int, @ColorInt endColor: Int
): Animator {
    val animator: ObjectAnimator
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        animator = ObjectAnimator.ofArgb(target, propertyName, startColor, endColor)
    } else {
        animator = ObjectAnimator.ofInt(target, propertyName, startColor, endColor)
        animator.setEvaluator(ArgbEvaluator())
    }

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        animator.interpolator = PathInterpolator(0.4f, 0f, 1f, 1f)
    }
    animator.duration = 1000
    return animator
}

var i = 0f

fun rotateImageLeft(activity: Activity, uri: Uri): Bitmap? {
    val matrix = Matrix()
    i -= 90f
    matrix.postRotate(i)
    return getRotatedBitmap(matrix, activity, uri)
}

fun rotateImageRight(activity: Activity, uri: Uri): Bitmap? {
    val matrix = Matrix()
    i += 90f
    matrix.preRotate(i)
    return getRotatedBitmap(matrix, activity, uri)
}

private fun getRotatedBitmap(
    matrix: Matrix,
    activity: Activity,
    uri: Uri
): Bitmap? {
    var rotated: Bitmap? = null
    try {
        val parcelFileDescriptor = activity.contentResolver?.openFileDescriptor(uri, "r")
        val fileDescriptor = parcelFileDescriptor?.fileDescriptor
        val image = BitmapFactory.decodeFileDescriptor(fileDescriptor)
        Thread().run {
            rotated = Bitmap.createBitmap(
                image, 0, 0, image.width, image.height,
                matrix, true
            )
        }
        parcelFileDescriptor?.close()
    } catch (e: IOException) {
        e.printStackTrace()
    }
    return rotated
}

fun getImageUriFromBitmap(context: Context, bitmap: Bitmap): Uri {
    val bytes = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
    val path = MediaStore.Images.Media.insertImage(context.contentResolver, bitmap, "Title", null)
    return Uri.parse(path.toString())
}
