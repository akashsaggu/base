package com.example.base.ui.edit.vm

import com.example.base.base.BaseViewModel
import com.example.base.networking.model.CommonResponse
import com.example.base.networking.model.UserModel
import com.example.base.networking.postModel.ProfilePicModel
import com.example.base.networking.toMultiPart
import gurtek.mrgurtekbase.viewmodel.VolatileLiveData
import java.io.File

class EditProfileVm : BaseViewModel() {


    private val commonLd by lazy { VolatileLiveData<CommonResponse>() }
    val commonObserver: VolatileLiveData<CommonResponse>
        get() {
            return commonLd
        }

    private val userDataLd by lazy { VolatileLiveData<UserModel.Data>() }
    val userObservers: VolatileLiveData<UserModel.Data>
        get() {
            return userDataLd
        }


    fun addEmail(email: String) {
        getRemoteRepo().addEmail(
            "uid",
            email
        ).subscribeWithProgressAndDisposable {
            userDataLd.setValue(it.data)
        }
    }

    fun addProfilePic(profilePic: File) {
        getRemoteRepo().uploadProfilePic(
            ProfilePicModel(
                "uid",
                profilePic
            ).toMultiPart()
        ).subscribeWithProgressAndDisposable {
            userDataLd.setValue(it.data)
        }
    }


}