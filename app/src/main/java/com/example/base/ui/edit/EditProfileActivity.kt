package com.example.base.ui.edit

import android.os.Bundle
import androidx.lifecycle.Observer
import com.example.base.R
import com.example.base.ui.edit.vm.EditProfileVm
import com.example.base.util.extension.toast
import gurtek.mrgurtekbase.KotlinBaseActivity
import kotlinx.android.synthetic.main.activity_edit_profile.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class EditProfileActivity : KotlinBaseActivity() {

    private val viewModel by viewModel<EditProfileVm>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        setViewModel(viewModel)
        setViews()
        setObservers()
    }

    private fun setViews() {
        setClickListeners()
    }

    private fun setObservers() {
        viewModel.userObservers.observe(this, Observer {
            toast("${it.uFirstName} logged in")
        })
    }

    private fun setClickListeners() {
        container.setOnClickListener {
            if (isValidEmail()) {
                viewModel.addEmail("Email@gmail.com")
            } else {
                showInfoAlert("Enter valid email.")
            }
        }
    }

    private fun isValidEmail(): Boolean {
        return true
    }
}
