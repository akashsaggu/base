package com.example.base.repository

import com.example.base.networking.ApiInterface
import com.example.base.networking.apiHitter
import com.example.base.networking.model.UserModel
import com.example.base.util.rxkotlin.onNetwork
import io.reactivex.Single
import okhttp3.RequestBody

class RemoteRepository : ApiInterface {
    override fun uploadProfilePic(request: RequestBody): Single<UserModel> {
        return apiHitter().uploadProfilePic(request).onNetwork()
    }

    override fun addEmail(userId: String, userEmail: String): Single<UserModel> {
        return apiHitter().addEmail(userId, userEmail).onNetwork()
    }


}
