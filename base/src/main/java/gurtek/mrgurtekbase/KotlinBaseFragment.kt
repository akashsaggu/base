package gurtek.mrgurtekbase

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.annotation.Keep
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import gurtek.mrgurtekbase.listeners.KotlinBaseListener
import gurtek.mrgurtekbase.viewmodel.KotlinBaseViewModel
import gurtek.mrgurtekbase.viewmodel.common.ErrorType
import kotlin.reflect.KClass

/**
 * * Created by Gurtek Singh on 1/1/2018.
 * Sachtech Solution
 * gurtek@protonmail.ch
 */
@Keep
abstract class KotlinBaseFragment(@LayoutRes val view: Int = 0) : Fragment() {

    protected lateinit var baseListener: KotlinBaseListener
    private var baseViewModel: KotlinBaseViewModel? = null

    fun setViewModel(vm: KotlinBaseViewModel) {
        baseViewModel = vm
        activity?.lifecycle?.addObserver(baseViewModel!!)
        observeProgress()
        listenForError()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is KotlinBaseListener) {
            baseListener = context
        } else {
            throw IllegalStateException("You Must have to extends your activity with KotlinBaseActivity")
        }
    }


    fun forceHideKeyboard() {
        (activity as KotlinBaseActivity).forceHideKeyboard()
    }

    override fun onDestroyView() {
        hideProgress()
        super.onDestroyView()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(view, container, false)
    }

    fun showDialog(
        clazz: KClass<out KotlinBaseDialogFragment>,
        bundle: Bundle? = Bundle(),
        show: Boolean = true
    ): Fragment? {
        val tag = clazz.java.simpleName
        val ft = fragmentManager?.beginTransaction()
        var fragment = fragmentManager?.findFragmentByTag(tag)
        if (fragment != null) {
            ft?.remove(fragment)
        }
        ft?.addToBackStack(tag)

        // Create and show the dialog.
        fragment = clazz.java.newInstance()
        fragment.arguments = bundle
        if (show) fragment.show(ft!!, tag)

        return fragment
    }

    @DrawableRes
    open fun setBreadCrumbsImage(): Int? {
        return null
    }

    open fun setBreadCrumbsTitle(): String {
        return ""
    }

    fun showProgress() {
        baseListener.showProgress()
    }

    fun hideProgress() {
        baseListener.hideProgress()
    }

    fun onBackPressed() {
        activity?.onBackPressed()
    }

    private fun observeProgress() {
        baseViewModel?.progressObserver?.observe(this, androidx.lifecycle.Observer {
            when (it) {
                true -> showProgress()
                false -> hideProgress()
            }
        })
    }

    fun showAlert(msg: String?) {
        (activity as? KotlinBaseActivity)?.showAlert(
            message = msg ?: "Sorry, something went wrong."
        )
    }

    fun showInfoAlert(msg: String) {
        (activity as? KotlinBaseActivity)?.showInfoAlert(
            message = msg
        )
    }

    private fun listenForError() {
        baseViewModel?.errorObserver?.observe(this, androidx.lifecycle.Observer {
            when (it.errorType) {
                ErrorType.SNACKBAR -> {
                    showAlert(it.msg)
                }//snack(it.msg ?: "")
                ErrorType.DIALOG -> activity?.showAlert(it.msg)
            }
        })
    }


}